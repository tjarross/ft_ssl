# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/19 12:34:44 by tjarross          #+#    #+#              #
#    Updated: 2018/08/15 12:54:30 by tjarross         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

EXEC =		ft_ssl
SRC =		src/ciphers/utils/pbkdf.c \
			src/ciphers/utils/format_hash.c \
			src/ciphers/utils/pad_message.c \
			src/ciphers/utils/buf_to_little_endian.c \
			src/ciphers/base64.c \
			src/ciphers/des.c \
			src/ciphers/md5.c \
			src/ciphers/sha224.c \
			src/ciphers/sha256.c \
			src/ciphers/sha384.c \
			src/ciphers/sha512.c \
			src/parsing/opts.c \
			src/parsing/parse_opts.c \
			src/utils/atoi_base.c \
			src/utils/get_file.c \
			src/utils/itoa_base.c \
			src/utils/strccpy.c \
			src/utils/strcdup.c \
			src/utils/strsplit.c \
			src/main.c
OBJ =		$(SRC:%.c=%.o)
INC =		-I include/ \
			-I libft/include/
LIB =		libft/libft.a -lbsd -lm -lcrypto
CC =		gcc
FLAGS =		-g -Wall -Wextra

all: $(EXEC)

msg:
	@echo "\033[0;29m⌛  Making Project : \c"

$(EXEC): msg $(OBJ)
	@echo ""
	@make -C libft/ -j8
	@$(CC) -o $(EXEC) $(OBJ) $(LIB)
	@echo "\033[0;34m✅  Executable created !\033[0;29m"

src/%.o: src/%.c
	@$(CC) $(FLAGS) -o $@ -c $< $(INC)
	@echo "\033[0;32m.\c\033[0;29m"

clean:
	@echo "\033[0;31m🔥  Cleaning Project Objects...\033[0;29m"
	@make -C libft/ clean
	@rm -rf $(OBJ)

fclean: clean
	@echo "\033[0;31m🔥  Cleaning Project Executable...\033[0;29m"
	make -C libft/ fclean
	@rm -rf $(EXEC)

re: fclean all

.PHONY: all clean fclean re msg
