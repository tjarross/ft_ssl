/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 12:32:32 by tjarross          #+#    #+#             */
/*   Updated: 2018/08/15 16:53:30 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include <ctype.h>
# include <byteswap.h>
# include <inttypes.h>
# include <bsd/readpassphrase.h>
# include <linux/limits.h>

# include "libft.h"

# define ROTLEFT_32(a, b)	(((a) << (b)) | ((a) >> (32 - (b))))
# define ROTRIGHT_32(a, b)	(((a) >> (b)) | ((a) << (32 - (b))))

# define ROTLEFT_64(a, b)	(((a) << (b)) | ((a) >> (64 - (b))))
# define ROTRIGHT_64(a, b)	(((a) >> (b)) | ((a) << (64 - (b))))

# define CH(x,y,z)		(((x) & (y)) ^ (~(x) & (z)))
# define MAJ(x,y,z)		(((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))

# define SHA256_EP0(x)	(ROTRIGHT_32(x, 2 ) ^ ROTRIGHT_32(x, 13) ^ ROTRIGHT_32(x, 22))
# define SHA256_EP1(x)	(ROTRIGHT_32(x, 6 ) ^ ROTRIGHT_32(x, 11) ^ ROTRIGHT_32(x, 25))
# define SHA256_SIG0(x)	(ROTRIGHT_32(x, 7 ) ^ ROTRIGHT_32(x, 18) ^ ((x) >> 3))
# define SHA256_SIG1(x)	(ROTRIGHT_32(x, 17) ^ ROTRIGHT_32(x, 19) ^ ((x) >> 10))

# define SHA512_EP0(x)	(ROTRIGHT_64(x, 28) ^ ROTRIGHT_64(x, 34) ^ ROTRIGHT_64(x, 39))
# define SHA512_EP1(x)	(ROTRIGHT_64(x, 14) ^ ROTRIGHT_64(x, 18) ^ ROTRIGHT_64(x, 41))
# define SHA512_SIG0(x)	(ROTRIGHT_64(x, 1 ) ^ ROTRIGHT_64(x, 8 ) ^ ((x) >> 7))
# define SHA512_SIG1(x)	(ROTRIGHT_64(x, 19) ^ ROTRIGHT_64(x, 61) ^ ((x) >> 6))

# define NB_DES_ROUNDS	16
# define NB_DES_KEYS	NB_DES_ROUNDS

# define STR(x)			#x
# define XSTR(x)		STR((x))
# define MAX_PWD_SIZE	64
# define MAX_CMD_SIZE	16
# define SUPPORTED_HASH_LIST	\
	{				\
		"md5",		\
		"sha224",	\
		"sha256",	\
		"sha384",	\
		"sha512",	\
	}
# define SUPPORTED_CIPHER_LIST	\
	{					\
		"base64",		\
		"base64url",	\
		"des-ecb",		\
	}
# define NB_ELEM_IN_LIST(list)	(sizeof((list)) / sizeof((*list)))
//# define MIN(x, y)				((x) > (y) ? (y) : (x))

typedef struct	s_parser
{
	char		*cmd;
	char		*short_options;
	char		*long_options;
	bool		permissive;
}				t_parser;

typedef enum	e_mode
{
	NO_MODE,
	ENCRYPT,
	DECRYPT
}				t_mode;

typedef enum	e_arg
{
	ARG_NONE,
	ARG_STRING,
	ARG_FILE,
}				t_arg;

typedef struct		s_opts
{
	char			cmd[MAX_CMD_SIZE + 1];
	t_mode			mode;
	char			in_pathname[PATH_MAX + 1];
	char			out_pathname[PATH_MAX + 1];
	unsigned char	*pbkdf2_key;
	size_t			pbkdf2_key_len;
	char			*salt;
	char			*enc_passwd;
	char			*iv;
	bool			a;
	bool			p;
	bool			P;
	bool			q;
	bool			r;
	bool			nopad;
	char			**arg;
	t_arg			*arg_type;
	size_t			nb_arg;
}					t_opts;

typedef struct		s_md5_ctx
{
	unsigned int	h[4];
	unsigned int	a;
	unsigned int	b;
	unsigned int	c;
	unsigned int	d;
	unsigned int	f;
	unsigned int	g;
	unsigned int	*w;
	unsigned int	k[64];
	unsigned int	r[64];
}					t_md5_ctx;

typedef struct		s_sha256_ctx
{
	unsigned int	h[8];
	unsigned int	a;
	unsigned int	b;
	unsigned int	c;
	unsigned int	d;
	unsigned int	e;
	unsigned int	f;
	unsigned int	g;
	unsigned int	h_;
	unsigned int	k[64];
	unsigned int	w[64];
}					t_sha256_ctx;

typedef t_sha256_ctx	t_sha224_ctx;

typedef struct		s_sha512_ctx
{
	unsigned long	h[8];
	unsigned long	a;
	unsigned long	b;
	unsigned long	c;
	unsigned long	d;
	unsigned long	e;
	unsigned long	f;
	unsigned long	g;
	unsigned long	h_;
	unsigned long	k[80];
	unsigned long	w[80];
}					t_sha512_ctx;

typedef struct			s_des_ctx
{
    char				*algorithm;
    int					mode;
	bool				padding;
    uint64_t			key;
    unsigned char		*plaintext;
    size_t				plaintext_size;
    unsigned char		*cipheredtext;
    size_t				cipheredtext_size;
}						t_des_ctx;

typedef t_sha512_ctx	t_sha384_ctx;

int				check_opts(char **options, int nb_options, t_parser *opts);
t_opts			parse_opts(int ac, char **av);

unsigned char	*md5(unsigned char *message, size_t byte_len);
unsigned char	*sha224(unsigned char *str, size_t size);
unsigned char	*sha256(unsigned char *str, size_t size);
unsigned char	*sha384(unsigned char *str, size_t size);
unsigned char	*sha512(unsigned char *str, size_t size);
unsigned char	*base64_encode(unsigned char *message, size_t size, char *type);
unsigned char	*base64_decode(unsigned char *message, size_t *size);
unsigned char   *des_ecb(t_des_ctx *ctx);
unsigned char	*pbkdf2_sha1(unsigned char *password, size_t password_len, unsigned char *salt, size_t salt_len, int iter, size_t out_key_size);

unsigned char   *format_hash(void *h, size_t tab_size, size_t format_size, size_t h_size);
unsigned char	*pad_message(unsigned char *message, size_t byte_len, size_t *padded_len, size_t block_size, size_t len_size);
void			buf_to_little_endian(void *data, size_t size);
unsigned long	little_endian64(unsigned long x);

int				get_file(char *pathname, size_t *len, char **str);
char			*itoa_base(unsigned long long value, int base, size_t out_len);
int				atoi_base(char *str, int base, size_t len);
unsigned int	rotate_left(unsigned int value, unsigned int shift);
unsigned int	rotate_right(unsigned int value, unsigned int shift);
unsigned int	little_endian(unsigned int x);
char			*strccpy(char *dest, const char *src, char c);
char			*strcdup(const char *s1, char c);
char			**strsplit(const char *str, char delim);
void			free_split(char **tab);
int				splitlen(char **split);

#endif
