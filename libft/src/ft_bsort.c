/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bsort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 16:13:32 by tjarross          #+#    #+#             */
/*   Updated: 2016/10/02 16:13:32 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	*ft_bsort(int *tab, int size)
{
	int x;
	int y;

	y = 0;
	while (y < (size - 1))
	{
		x = 0;
		while (x < (size - y - 1))
		{
			if (tab[x] > tab[x + 1])
				ft_swap(&tab[x], &tab[x + 1]);
			x++;
		}
		y++;
	}
	return (tab);
}
