/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 23:02:55 by tjarross          #+#    #+#             */
/*   Updated: 2016/03/16 23:02:55 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_cpytocc(char *str, char c)
{
	char	*ret;
	int		i;

	i = 0;
	while (str[i] != c)
		i++;
	ret = (char *)malloc(sizeof(char) * i);
	ret[i - 1] = 0;
	i = -1;
	while (str[++i] != c)
		ret[i] = str[i];
	return (ret);
}
