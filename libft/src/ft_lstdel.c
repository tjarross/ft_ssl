/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:50:06 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:50:06 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *lst;
	t_list *current;

	lst = *alst;
	while (lst)
	{
		current = lst;
		lst = lst->next;
		del(current->content, current->content_size);
		free(current);
	}
	*alst = NULL;
}
