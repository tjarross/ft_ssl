/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:49:53 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:49:53 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
	t_list *lst;

	if (alst && *alst)
	{
		lst = *alst;
		*alst = lst->next;
		del(lst->content, lst->content_size);
		free(lst);
		*alst = NULL;
	}
}
