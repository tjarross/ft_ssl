/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:50:38 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:50:38 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *link;
	t_list *current;
	t_list *new;

	if (!lst || !f)
		return (NULL);
	link = NULL;
	current = lst;
	while (lst)
	{
		new = (*f)(lst);
		if (link)
		{
			current->next = new;
			current = current->next;
		}
		else
		{
			link = new;
			current = link;
		}
		lst = lst->next;
	}
	return (link);
}
