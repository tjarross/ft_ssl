/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:49:36 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:49:36 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list		*new;
	void		*value;
	size_t		cpysize;

	new = (t_list *)malloc(sizeof(t_list));
	if (!new)
		return (NULL);
	if (!content)
	{
		new->content = NULL;
		new->content_size = 0;
	}
	else
	{
		value = ft_memalloc(content_size);
		ft_memcpy(value, content, content_size);
		cpysize = content_size;
		new->content = value;
		new->content_size = cpysize;
	}
	new->next = NULL;
	return (new);
}
