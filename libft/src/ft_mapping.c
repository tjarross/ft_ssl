/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapping.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 20:29:58 by tjarross          #+#    #+#             */
/*   Updated: 2016/03/15 13:35:24 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_mapping(int size)
{
	char	**map;
	int		y;

	map = (char **)malloc(sizeof(char *) * size + 1);
	y = 0;
	while (y < size)
		map[y++] = ft_strnew(size);
	map[size] = NULL;
	return (map);
}
