/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memswap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 01:56:36 by tjarross          #+#    #+#             */
/*   Updated: 2016/11/01 01:56:36 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void ft_memswap(void *a, void *b, size_t size)
{
	size_t	i;

	i = -1;
	while (++i < size)
	{
		if (((char *)a)[i] != ((char *)b)[i])
		{
			((char *)a)[i] ^= ((char *)b)[i];
			((char *)b)[i] ^= ((char *)a)[i];
			((char *)a)[i] ^= ((char *)b)[i];
		}
	}
}
