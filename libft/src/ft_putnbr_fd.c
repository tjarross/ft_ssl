/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:54:11 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:54:11 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_rec_putnbr_fd(int n, int fd)
{
	if ((n / 10) > 0)
		ft_rec_putnbr_fd(n / 10, fd);
	ft_putchar_fd((n % 10) + '0', fd);
}

void		ft_putnbr_fd(int n, int fd)
{
	if (n == (-2147483647 - 1))
		ft_putstr_fd("-2147483648", fd);
	else
	{
		if (n < 0 && (n = -n))
			ft_putchar_fd('-', fd);
		ft_rec_putnbr_fd(n, fd);
	}
}
