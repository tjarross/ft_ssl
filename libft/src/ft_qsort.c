/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qsort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 16:13:23 by tjarross          #+#    #+#             */
/*   Updated: 2016/10/02 16:13:23 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	*ft_sort(int *tab, int beg, int end)
{
	int			left;
	int			right;
	int			pivot;

	left = beg - 1;
	right = end + 1;
	pivot = tab[beg];
	if (beg >= end)
		return (tab);
	while (1)
	{
		while (tab[--right] > pivot)
			;
		while (tab[++left] < pivot)
			;
		if (left < right)
			ft_swap(&tab[left], &tab[right]);
		else
			break ;
	}
	ft_sort(tab, beg, right);
	ft_sort(tab, right + 1, end);
	return (tab);
}

int			*ft_qsort(int *tab, int size)
{
	return (ft_sort(tab, 0, size));
}
