#include "libft.h"

void	*ft_realloc(void *ptr, size_t ptr_size, size_t size)
{
	void	*new_ptr;

	if (ptr == NULL)
		return (malloc(size));
	else if (size == 0)
		free(ptr);
	else
	{
		if (!(new_ptr = malloc(size)))
			return (NULL);
		ft_memcpy(new_ptr, ptr, ptr_size);
		free(ptr);
		return (new_ptr);
	}
	return (NULL);
}
