/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:33:07 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:33:07 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	char	*d;
	int		i;
	int		j;

	d = s1;
	i = 0;
	j = 0;
	while (d[i])
		i++;
	while (n-- && (d[i] = s2[j]))
	{
		i++;
		j++;
	}
	d[i] = '\0';
	return (s1);
}
