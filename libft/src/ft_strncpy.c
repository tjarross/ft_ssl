/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:32:24 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:32:24 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	char	*d;
	size_t	i;

	d = (char *)dst;
	i = 0;
	while (*src && i < n)
	{
		*d++ = *src++;
		i++;
	}
	while (i < n)
	{
		*d++ = '\0';
		i++;
	}
	return (dst);
}
