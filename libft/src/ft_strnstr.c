/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:34:37 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:34:37 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	k;
	size_t	i;
	size_t	j;

	i = 0;
	if (s2[0] == '\0')
		return ((char *)s1);
	while (s1[i] && i < n)
	{
		j = 0;
		k = 0;
		if (s2[0] == s1[i])
		{
			k = i;
			while (s1[k] == s2[j] && k < n)
			{
				if (j == ft_strlen(s2) - 1)
					return ((char *)&s1[k - j]);
				k++;
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
