/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:34:07 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:34:07 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int		k;
	int		i;
	size_t	j;

	i = 0;
	if (s2[0] == '\0')
		return ((char *)s1);
	while (s1[i])
	{
		j = 0;
		k = 0;
		if (s2[0] == s1[i])
		{
			k = i;
			while (s1[k] == s2[j])
			{
				if (j == ft_strlen(s2) - 1)
					return ((char *)&s1[k - j]);
				k++;
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
