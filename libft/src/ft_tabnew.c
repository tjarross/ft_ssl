/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:50:54 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:50:54 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		*ft_tabnew(size_t size)
{
	int *tab;

	tab = (int *)malloc(sizeof(int) * size);
	if (!tab)
		return (NULL);
	size--;
	while (size)
		tab[size--] = 0;
	return (tab);
}
