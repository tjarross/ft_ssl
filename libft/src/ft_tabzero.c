/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabzero.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:51:02 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:51:02 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_tabzero(int *tab, size_t tab_len, size_t n)
{
	if (n <= tab_len)
	{
		while (n--)
			tab[n] = 0;
	}
	else
	{
		while (tab_len--)
			tab[tab_len] = 0;
	}
}
