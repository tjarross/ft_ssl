#include "libft.h"

void	ft_wait(int milliseconds)
{
	clock_t	ticks1;
	clock_t	ticks2;

	ticks1 = clock();
	ticks2 = ticks1;
	while((ticks2 / (float)CLOCKS_PER_SEC
		- ticks1 / (float)CLOCKS_PER_SEC) < 1 / (float)milliseconds)
		ticks2=clock();
}
