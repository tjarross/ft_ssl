#include "ft_ssl.h"

static char			encode_char(char c, char *type)
{
	if (c < 26)
		return ('A' + c);
	else if (c < 52)
		return ('a' + c - 26);
	else if (c < 62)
		return ('0' + c - 52);
	else if (c == 62)
		return ((strcmp(type, "base64url") ? '-' : '+'));
	else
		return ((strcmp(type, "base64url") ? '_' : '/'));
}

static char			decode_char(unsigned char **message, int index, size_t *size)
{
	if ((*message)[index] == '\n')
	{
		(*size)--;
		(*message)++;
	}
	if (isupper((*message)[index]))
		return ((*message)[index] - 'A');
	else if (islower((*message)[index]))
		return ((*message)[index] - 'a' + 26);
	else if (isdigit((*message)[index]))
		return ((*message)[index] - '0' + 52);
	else if ((*message)[index] == '+' || (*message)[index] == '-')
		return (62);
	else if ((*message)[index] == '/' || (*message)[index] == '_')
		return (63);
	return (0);
}

unsigned char	*base64_encode(unsigned char *message, size_t size, char *type)
{
	unsigned char	*str;
	int				encode_size;
	int				tmp;
	int				i;

	if (size == 0)
		return (NULL);
	encode_size = (((size * 4) / 3) + 3) & ~3;
	if (!(str = (unsigned char *)malloc(sizeof(unsigned char) * encode_size + 1)))
		return (NULL);
	str[encode_size + 1] = '\0';
	i = 0;
	while (size >= 3)
	{
		tmp = (message[0] << 16 ) | (message[1] << 8) | (message[2]);

		str[i++] = encode_char(tmp >> 18, type);
		str[i++] = encode_char((tmp >> 12) & 0x3f, type);
		str[i++] = encode_char((tmp >> 6) & 0x3f, type);
		str[i++] = encode_char(tmp & 0x3f, type);
		message += 3;
		size -= 3;
	}
	if (size == 2)
	{
		tmp = (message[0] << 8) | (message[1]);
		tmp <<= 2;
		str[i++] = encode_char((tmp >> 12) & 0x3f, type);
		str[i++] = encode_char((tmp >> 6) & 0x3f, type);
		str[i++] = encode_char(tmp & 0x3f, type);
		str[i++] = '=';
	}
	else if (size == 1)
	{
		tmp = message[0] << 4;
		str[i++] = encode_char((tmp >> 6) & 0x3f, type);
		str[i++] = encode_char(tmp & 0x3f, type);
		str[i++] = '=';
		str[i++] = '=';
	}
	return (str);
}

static int				get_decode_size(const unsigned char *message)
{
	int max_len;
	int msg_len;
	int	len;

	msg_len = strlen((const char *)message);
	max_len = msg_len / 4 * 3;
	len = max_len;
	if (message[msg_len - 1] == '=')
		--len;
	if (message[msg_len - 2] == '=')
		--len;
	return (len);
}

int	occurrence(char *str, char c)
{
	int i;
	int cpt;

	i = 0;
	cpt = 0;
	while (str[i])
	{
		if (str[i] == c)
			cpt++;
		++i;
	}
	return (cpt);
}

static int	check_newlines_integrity(unsigned char *message)
{
	int i;

	while (message[i])
	{
		if (message[i] == '\n' && i % 64 != 0)
			return (1);
		++i;
	}
	return (0);
}

unsigned char	*base64_decode(unsigned char *message, size_t *size)
{
	unsigned char	*str;
	int				decode_size;
	int				tmp;
	int				i;

	if ((*size - occurrence((char *)message, '\n')) % 4 != 0)
		return (NULL);
	decode_size = get_decode_size(message);
	if (!(str = (unsigned char *)malloc(sizeof(unsigned char) * decode_size + 1)))
		return (NULL);
	if (check_newlines_integrity(message))
		return (NULL);
	str[decode_size] = '\0';
	i = 0;
	while (*size > 4)
	{
		tmp  = decode_char(&message, 0, size) << 18;
		tmp |= decode_char(&message, 1, size) << 12;
		tmp |= decode_char(&message, 2, size) << 6;
		tmp |= decode_char(&message, 3, size);

		str[i++] = tmp >> 16;
		str[i++] = tmp >> 8;
		str[i++] = tmp & 0xff;

		*size -= 4;
		message += 4;
		if (*size % 64 == 0 && *message == '\n')
		{
			*size--;
			message++;
		}
	}
	
	tmp = 0;
	while (*size > 0)
	{
		if (message[0] != '=')
			tmp |= decode_char(&message, 0, size);
		tmp <<= 6;
		size--;
		message++;
	}
	tmp >>= 6;

	str[i++] = tmp >> 16;
	str[i++] = tmp >> 8;
	str[i++] = tmp & 0xff;

	*size = i;
	return (str);
}
