#include "ft_ssl.h"

#define ROTLEFT_28(a, b)	(((a) << (b)) | ((a) >> (28 - (b))))

#define ALIGN8(x)			(((((x) - 1) >> 3) << 3) + 8)

// Initial permutation
const unsigned int  ip[64] =
{
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17, 9,  1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7,
};

// Final permutation
const unsigned int  ip_1[64] =
{
    40, 8, 48, 16, 56, 24, 64, 32,
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41, 9,  49, 17, 57, 25,
};

// Expansion function
const unsigned int  e[48] =
{
    32, 1,  2,  3,  4,  5,
    4,  5,  6,  7,  8,  9,
    8,  9,  10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32, 1,
};

// Permutation
const unsigned int  p[32] =
{
    16, 7,  20, 21, 29, 12, 28, 17,
    1,  15, 23, 26, 5,  18, 31, 10,
    2,  8,  24, 14, 32, 27, 3,  9,
    19, 13, 30, 6,  22, 11, 4,  25,
};

// Permuted choice 1
const unsigned int  pc_1[56] =
{
    57, 49, 41, 33, 25, 17, 9,
    1,  58, 50, 42, 34, 26, 18,
    10, 2,  59, 51, 43, 35, 27,
    19, 11, 3,  60, 52, 44, 36,
    63, 55, 47, 39, 31, 23, 15,
    7,  62, 54, 46, 38, 30, 22,
    14, 6,  61, 53, 45, 37, 29,
    21, 13, 5,  28, 20, 12, 4,
};

// Permuted choice 2 
const unsigned int  pc_2[48] =
{
    14, 17, 11, 24, 1,  5,
    3,  28, 15, 6,  21, 10,
    23, 19, 12, 4,  26, 8,
    16, 7,  27, 20, 13, 2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32,
};

// S-Box 1
const unsigned int  sbox1[64] =
{
    14, 4,  13, 1, 2,  15, 11, 8,  3,  10, 6,  12, 5,  9,  0, 7,
    0,  15, 7,  4, 14, 2,  13, 1,  10, 6,  12, 11, 9,  5,  3, 8,
    4,  1,  14, 8, 13, 6,  2,  11, 15, 12, 9,  7,  3,  10, 5, 0,
    15, 12, 8,  2, 4,  9,  1,  7,  5,  11, 3,  14, 10, 0,  6, 13,
};

// S-Box 2
const unsigned int  sbox2[64] =
{
    15, 1,  8,  14, 6,  11, 3,  4,  9,  7, 2,  13, 12, 0, 5,  10,
    3,  13, 4,  7,  15, 2,  8,  14, 12, 0, 1,  10, 6,  9, 11, 5,
    0,  14, 7,  11, 10, 4,  13, 1,  5,  8, 12, 6,  9,  3, 2,  15,
    13, 8,  10, 1,  3,  15, 4,  2,  11, 6, 7,  12, 0,  5, 14, 9,
};

// S-Box 3
const unsigned int  sbox3[64] =
{
    10, 0,  9,  14, 6, 3,  15, 5,  1,  13, 12, 7,  11, 4,  2,  8,
    13, 7,  0,  9,  3, 4,  6,  10, 2,  8,  5,  14, 12, 11, 15, 1,
    13, 6,  4,  9,  8, 15, 3,  0,  11, 1,  2,  12, 5,  10, 14, 7,
    1,  10, 13, 0,  6, 9,  8,  7,  4,  15, 14, 3,  11, 5,  2,  12,
};

// S-Box 4
const unsigned int  sbox4[64] =
{
    7,  13, 14, 3, 0,  6,  9,  10, 1,  2, 8, 5,  11, 12, 4,  15,
    13, 8,  11, 5, 6,  15, 0,  3,  4,  7, 2, 12, 1,  10, 14, 9,
    10, 6,  9,  0, 12, 11, 7,  13, 15, 1, 3, 14, 5,  2,  8,  4,
    3,  15, 0,  6, 10, 1,  13, 8,  9,  4, 5, 11, 12, 7,  2,  14,
};

// S-Box 5
const unsigned int  sbox5[64] =
{
    2,  12, 4,  1,  7,  10, 11, 6,  8,  5,  3,  15, 13, 0, 14, 9,
    14, 11, 2,  12, 4,  7,  13, 1,  5,  0,  15, 10, 3,  9, 8,  6,
    4,  2,  1,  11, 10, 13, 7,  8,  15, 9,  12, 5,  6,  3, 0,  14,
    11, 8,  12, 7,  1,  14, 2,  13, 6,  15, 0,  9,  10, 4, 5,  3,
};

// S-Box 6
const unsigned int  sbox6[64] =
{
    12, 1,  10, 15, 9, 2,  6,  8,  0,  13, 3,  4,  14, 7,  5,  11,
    10, 15, 4,  2,  7, 12, 9,  5,  6,  1,  13, 14, 0,  11, 3,  8,
    9,  14, 15, 5,  2, 8,  12, 3,  7,  0,  4,  10, 1,  13, 11, 6,
    4,  3,  2,  12, 9, 5,  15, 10, 11, 14, 1,  7,  6,  0,  8,  13,
};

// S-Box 7
const unsigned int  sbox7[64] =
{
    4,  11, 2,  14, 15, 0, 8,  13, 3,  12, 9, 7,  5,  10, 6, 1,
    13, 0,  11, 7,  4,  9, 1,  10, 14, 3,  5, 12, 2,  15, 8, 6,
    1,  4,  11, 13, 12, 3, 7,  14, 10, 15, 6, 8,  0,  5,  9, 2,
    6,  11, 13, 8,  1,  4, 10, 7,  9,  5,  0, 15, 14, 2,  3, 12,
};

// S-Box 8
const unsigned int  sbox8[64] =
{
    13, 2,  8,  4, 6,  15, 11, 1,  10, 9,  3,  14, 5,  0,  12, 7,
    1,  15, 13, 8, 10, 3,  7,  4,  12, 5,  6,  11, 0,  14, 9,  2,
    7,  11, 4,  1, 9,  12, 14, 2,  0,  6,  10, 13, 15, 3,  5,  8,
    2,  1,  14, 7, 4,  10, 8,  13, 15, 12, 9,  0,  3,  5,  6,  11,
};

// Subkey shifts
const unsigned int  key_shift[16] =
{
    1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1,
};

// Subkeys
uint64_t 	sub_keys[NB_DES_ROUNDS] = {0};

void printBits(unsigned long n, int bit)
{
    char bits[64 + 1] = {0};
    int i = 0;

    memset(bits, '0', 64);
    while (n)
    {
        if (n & 1)
            bits[i] = '1';
        else
            bits[i] = '0';
        n >>= 1;
        ++i;
    }
    for (int j = bit - 1; j >= 0; --j)
    {
        printf("%c", bits[j]);
    }
    printf(" ");
}

uint64_t	permute(uint64_t in, const unsigned int *box, size_t box_size)
{
    size_t          i;
    unsigned long   res;

    i = 0;
    res = 0;
    while (i < box_size)
    {
        res |= (in & (unsigned long)(1UL << (64 - box[i]))) >> (64 - box[i]) << (64 - (i + 1));
        ++i;
    }
    return (res >> (64 - box_size));
}

uint64_t	subkey_transform(unsigned int *key_right, unsigned int *key_left, int round_index)
{
    *key_right = ROTLEFT_28(*key_right, key_shift[round_index]);
    *key_left = ROTLEFT_28(*key_left, key_shift[round_index]);

    return (((uint64_t)*key_left << 28) | (*key_right & 0xFFFFFFF));
}

void            gen_subkeys(uint64_t key)
{
    unsigned int	key_right;
    unsigned int	key_left;
    int				i;

    key = permute(key, pc_1, 56);
    key_left = key >> 28;
    key_right = key & 0xFFFFFFF;

    i = 0;
    while (i < NB_DES_KEYS)
    {
        sub_keys[i] = permute(subkey_transform(&key_right, &key_left, i) << 8, pc_2, 48);
        ++i;
    }
}

int             get_sbox_position(uint8_t n)
{
    return (((((n & 0b100000) >> 4) | (n & 0b000001)) * 16) + ((n & 0b011110) >> 1));
}

unsigned int    feistel(unsigned long message, unsigned long sub_key)
{
    uint64_t		tmp;
    unsigned int	res;
    tmp = permute(message << 32, e, 48);
    tmp ^= sub_key;

    res  = sbox1[get_sbox_position((tmp >> 42))] << 28;
    res |= sbox2[get_sbox_position((tmp >> 36))] << 24;
    res |= sbox3[get_sbox_position((tmp >> 30))] << 20;
    res |= sbox4[get_sbox_position((tmp >> 24))] << 16;
    res |= sbox5[get_sbox_position((tmp >> 18))] << 12;
    res |= sbox6[get_sbox_position((tmp >> 12))] <<  8;
    res |= sbox7[get_sbox_position((tmp >>  6))] <<  4;
    res |= sbox8[get_sbox_position((tmp >>  0))] <<  0;
    return (permute((uint64_t)res << 32, p, 32));
}

uint64_t   des_round(uint64_t message, uint64_t sub_key)
{
    unsigned int    l0;
    unsigned int    r0;
    unsigned int    l1;
    unsigned int    r1;

    l0 = message >> 32;
    r0 = message & 0xFFFFFFFF;

    r1 = l0 ^ feistel(r0, sub_key);
    l1 = r0;
    return ((uint64_t)r1 | ((uint64_t)l1 << 32));
}

uint64_t   process_rounds(uint64_t message, const uint64_t *sub_keys, int mode)
{
    int i;

    i = 0;
    while (i < NB_DES_ROUNDS)
    {
        if (mode == ENCRYPT)
            message = des_round(message, sub_keys[i]);
        else
            message = des_round(message, sub_keys[15 - i]);
        ++i;
    }
    return ((message >> 32) | (message << 32));
}

unsigned char   *pkcs5_padding(unsigned char *plaintext, size_t *size)
{
    unsigned char   *padded;
    size_t          old_size;

    old_size = *size;
    *size = (*size % 8) ? (ALIGN8(*size)) : (*size + 8);

    if (!(padded = (unsigned char *)malloc(sizeof(unsigned char) * (*size + 1))))
        return (NULL);
    padded[*size] = '\0';
    memcpy(padded, plaintext, old_size);
    memset(padded + old_size, *size - old_size, *size - old_size);
    return (padded);
}

int             pkcs5_padding_verify(unsigned char *str, size_t size)
{
    size_t          i;
    unsigned char   c;
    int             cpt;

    i = size - 1;
    c = str[size - 1];
    if (size % 8 == 0 && c > '\x08')
        return (1);

    cpt = 0;
    while (str[i] == c)
    {
        if (i == 0)
        {
            ++cpt;
            break ;
        }
        ++cpt;
        --i;
    }
    return (cpt != c);
}

unsigned char   *des_transform(t_des_ctx *ctx, size_t *paddedtext_size)
{
    unsigned char   *paddedtext;

    if (ctx->mode == ENCRYPT)
    {
		if (ctx->padding == false)
			return (ctx->plaintext);
        if (!(paddedtext = pkcs5_padding(ctx->plaintext, paddedtext_size)))
            return (NULL);
    }
    else
        paddedtext = ctx->plaintext;

    return (paddedtext);
}

unsigned char   *des_update(t_des_ctx *ctx, const unsigned char *paddedtext, size_t paddedtext_size)
{
    uint64_t	tmp;
    size_t		i;

    if (!(ctx->cipheredtext = (unsigned char *)malloc(sizeof(unsigned char) * (paddedtext_size + 1))))
        return (NULL);
    gen_subkeys(ctx->key);
    i = 0;
    while (i < paddedtext_size)
    {
        tmp = permute(bswap_64(((uint64_t *)paddedtext)[i / 8]), ip, 64);
        tmp = process_rounds(tmp, sub_keys, ctx->mode);
        tmp = bswap_64(permute(tmp, ip_1, 64));
        memcpy(ctx->cipheredtext + i, (void *)&tmp, 8);
        i += 8;
    }
    ctx->cipheredtext_size = paddedtext_size;
    return (ctx->cipheredtext);
}

unsigned char *des_final(t_des_ctx *ctx, unsigned char *paddedtext)
{
    if (ctx->mode == ENCRYPT)
	{
		if (ctx->padding == true)
			free(paddedtext);
	}
    else
    {
        if (pkcs5_padding_verify(ctx->cipheredtext, ctx->cipheredtext_size))
        {
            free(ctx->cipheredtext);
			ctx->cipheredtext = NULL;
            ctx->cipheredtext_size = 0;
            return (NULL);
        }
        ctx->cipheredtext_size -= ctx->cipheredtext[ctx->cipheredtext_size - 1];
    }
    return (ctx->cipheredtext);
}

unsigned char   *des_ecb(t_des_ctx *ctx)
{
    unsigned char   *paddedtext;
    size_t          paddedtext_size;

    if (ctx->plaintext_size % 8 != 0)
		if (ctx->padding == false || ctx->mode == DECRYPT)
        	return (NULL);
    paddedtext_size = ctx->plaintext_size;
    paddedtext = des_transform(ctx, &paddedtext_size);
    ctx->cipheredtext = des_update(ctx, paddedtext, paddedtext_size);
    return (des_final(ctx, paddedtext));
}