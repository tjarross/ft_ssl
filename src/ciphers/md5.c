#include "ft_ssl.h"

static void		get_r(unsigned int *out)
{
	unsigned int	r[] =
	{
		7, 12, 17, 22, 7, 12, 17, 22,
		7, 12, 17, 22, 7, 12, 17, 22,
		5,  9, 14, 20, 5,  9, 14, 20,
		5,  9, 14, 20, 5,  9, 14, 20,
		4, 11, 16, 23, 4, 11, 16, 23,
		4, 11, 16, 23, 4, 11, 16, 23,
		6, 10, 15, 21, 6, 10, 15, 21,
		6, 10, 15, 21, 6, 10, 15, 21,
	};

	memcpy(out, r, sizeof(r));
}

static void		get_k(unsigned int *out)
{
	unsigned int	k[] =
	{
		0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
		0xf57c0faf,	0x4787c62a, 0xa8304613, 0xfd469501,
		0x698098d8,	0x8b44f7af,	0xffff5bb1, 0x895cd7be,
		0x6b901122,	0xfd987193,	0xa679438e,	0x49b40821,
		0xf61e2562, 0xc040b340, 0x265e5a51,	0xe9b6c7aa,
		0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
		0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
		0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
		0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
		0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
		0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
		0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
		0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
		0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
		0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
		0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391,
	};

	memcpy(out, k, sizeof(k));
}

static void		init_hash_values(unsigned int *h)
{
	h[0] = 0x67452301;
	h[1] = 0xefcdab89;
    h[2] = 0x98badcfe;
    h[3] = 0x10325476;
}

unsigned char	*md5(unsigned char *message, size_t byte_len)
{
	t_md5_ctx		ctx;
	unsigned char	*padded;
	size_t			padded_len;
	int				i;
	size_t			block_nb;
	int				tmp;

	init_hash_values(ctx.h);
	get_r(ctx.r);
	get_k(ctx.k);
	if (!(padded = pad_message(message, byte_len, &padded_len, 512, 64)))
		return (NULL);
	block_nb = 0;
	while (block_nb * 64 < padded_len)
	{
		ctx.w = (unsigned int *)(padded + block_nb * 64);
		ctx.a = ctx.h[0];
		ctx.b = ctx.h[1];
		ctx.c = ctx.h[2];
		ctx.d = ctx.h[3];
		i = -1;
		while (++i < 64)
		{
			if (i < 16)
			{
				ctx.f = (ctx.b & ctx.c) | ((~ctx.b) & ctx.d);
				ctx.g = i;
			}	
			else if (i < 32)
			{
				ctx.f = (ctx.d & ctx.b) | ((~ctx.d) & ctx.c);
				ctx.g = (5 * i + 1) % 16;
			}
			else if (i < 48)
			{
				ctx.f = ctx.b ^ ctx.c ^ ctx.d;
				ctx.g = (3 * i + 5) % 16;
			}
			else
			{
				ctx.f = ctx.c ^ (ctx.b | (~ctx.d));
				ctx.g = (7 * i) % 16;
			}
			tmp = ctx.d;
			ctx.d = ctx.c;
			ctx.c = ctx.b;
			ctx.b += ROTLEFT_32(ctx.a + ctx.f + ctx.k[i] + ctx.w[ctx.g], ctx.r[i]);
			ctx.a = tmp;
		}
		ctx.h[0] += ctx.a;
		ctx.h[1] += ctx.b;
		ctx.h[2] += ctx.c;
		ctx.h[3] += ctx.d;
		++block_nb;
	}
	free(padded);

	i = -1;
	while (++i < 4)
		ctx.h[i] = bswap_32(ctx.h[i]);

	return (format_hash(ctx.h, 4, 32, 8));
}
