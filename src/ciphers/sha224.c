#include "ft_ssl.h"

static void		fill_w(unsigned char *msg, unsigned int *w)
{
	int		t;
	int		i;

	t = 0;
	i = 0;
	while (t < 16)
	{
		w[t] = (msg[i + 0] << 24) | (msg[i + 1] << 16)
			|  (msg[i + 2] << 8 ) | (msg[i + 3] <<  0);
		i += 4;
		t++;
	}
	while (t < 64)
	{
		w[t] = SHA256_SIG1(w[t - 2]) + w[t - 7] + SHA256_SIG0(w[t - 15]) + w[t - 16];
		++t;
	}
}

static void		get_k(unsigned int *out)
{
	unsigned int	k[] =
	{
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
		0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
		0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
		0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
		0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
		0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
		0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
		0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
		0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
	};

	memcpy(out, k, sizeof(k));
}

static void		init_struct(t_sha224_ctx *ctx)
{
	ctx->h[0] = 0xc1059ed8;
	ctx->h[1] = 0x367cd507;
	ctx->h[2] = 0x3070dd17;
	ctx->h[3] = 0xf70e5939;
	ctx->h[4] = 0xffc00b31;
	ctx->h[5] = 0x68581511;
	ctx->h[6] = 0x64f98fa7;
	ctx->h[7] = 0xbefa4fa4;
}

unsigned char	*sha224(unsigned char *str, size_t size)
{
	unsigned char	*padded;
	size_t			padded_len;
	t_sha224_ctx	ctx;
	size_t			i;
	unsigned int	t, t1, t2;

	if (!(padded = pad_message(str, size, &padded_len, 512, 64)))
		return (NULL);
	buf_to_little_endian(padded + padded_len, 8);
	init_struct(&ctx);
	get_k(ctx.k);
	i = -1;
	while (++i * 64 < padded_len)
	{
		fill_w(padded + (i * 64), ctx.w);
		ctx.a = ctx.h[0];
		ctx.b = ctx.h[1];
		ctx.c = ctx.h[2];
		ctx.d = ctx.h[3];
		ctx.e = ctx.h[4];
		ctx.f = ctx.h[5];
		ctx.g = ctx.h[6];
		ctx.h_ = ctx.h[7];
		t = -1;
		while (++t < 64)
		{
			t1 = ctx.h_ + SHA256_EP1(ctx.e) + CH(ctx.e, ctx.f, ctx.g) + ctx.k[t] + ctx.w[t];
			t2 = SHA256_EP0(ctx.a) + MAJ(ctx.a, ctx.b, ctx.c);
			ctx.h_ = ctx.g;
			ctx.g = ctx.f;
			ctx.f = ctx.e;
			ctx.e = ctx.d + t1;
			ctx.d = ctx.c;
			ctx.c = ctx.b;
			ctx.b = ctx.a;
			ctx.a = t1 + t2;
		}
		ctx.h[0] += ctx.a;
		ctx.h[1] += ctx.b;
		ctx.h[2] += ctx.c;
		ctx.h[3] += ctx.d;
		ctx.h[4] += ctx.e;
		ctx.h[5] += ctx.f;
		ctx.h[6] += ctx.g;
		ctx.h[7] += ctx.h_;
	}
	free(padded);
	return (format_hash(ctx.h, 7, 56, 8));
}
