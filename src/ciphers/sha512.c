#include "ft_ssl.h"

static void		fill_w(unsigned char *msg, unsigned long *w)
{
	int		t;
	int		i;

	t = 0;
	i = 0;
	while (t < 16)
	{
		w[t] = ((unsigned long)msg[i + 0] << 56) | ((unsigned long)msg[i + 1] << 48)
			|  ((unsigned long)msg[i + 2] << 40) | ((unsigned long)msg[i + 3] << 32)
            |  ((unsigned long)msg[i + 4] << 24) | ((unsigned long)msg[i + 5] << 16)
            |  ((unsigned long)msg[i + 6] << 8 ) | ((unsigned long)msg[i + 7]);
		i += 8;
		t++;
	}
	while (t < 80)
	{
		w[t] = SHA512_SIG1(w[t - 2]) + w[t - 7] + SHA512_SIG0(w[t - 15]) + w[t - 16];
		++t;
	}
}

static void		get_k(unsigned long *out)
{
	static unsigned long	k[] =
	{
		0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
        0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
        0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
        0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
        0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
        0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
        0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
        0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
        0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
        0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
        0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
        0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
        0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
        0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
        0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
        0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
        0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
        0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
        0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
        0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817,
	};

	memcpy(out, k, sizeof(k));
}

static void		init_struct(t_sha512_ctx *ctx)
{
	ctx->h[0] = 0x6a09e667f3bcc908;
	ctx->h[1] = 0xbb67ae8584caa73b;
	ctx->h[2] = 0x3c6ef372fe94f82b;
	ctx->h[3] = 0xa54ff53a5f1d36f1;
	ctx->h[4] = 0x510e527fade682d1;
	ctx->h[5] = 0x9b05688c2b3e6c1f;
	ctx->h[6] = 0x1f83d9abfb41bd6b;
	ctx->h[7] = 0x5be0cd19137e2179;
}
unsigned char	*sha512(unsigned char *str, size_t size)
{
	unsigned char	*padded;
	size_t			padded_len;
	t_sha512_ctx	ctx;
	size_t			i;
	unsigned long	t, t1, t2;

	if (!(padded = pad_message(str, size, &padded_len, 1024, 128)))
		return (NULL);
	buf_to_little_endian(padded + padded_len, 16);
	init_struct(&ctx);
	get_k(ctx.k);
	i = -1;
	while (++i * 128 < padded_len)
	{
		fill_w(padded + (i * 128), ctx.w);
		ctx.a = ctx.h[0];
		ctx.b = ctx.h[1];
		ctx.c = ctx.h[2];
		ctx.d = ctx.h[3];
		ctx.e = ctx.h[4];
		ctx.f = ctx.h[5];
		ctx.g = ctx.h[6];
		ctx.h_ = ctx.h[7];
		t = -1;
		while (++t < 80)
		{
			t1 = ctx.h_ + SHA512_EP1(ctx.e) + CH(ctx.e, ctx.f, ctx.g) + ctx.k[t] + ctx.w[t];
			t2 = SHA512_EP0(ctx.a) + MAJ(ctx.a, ctx.b, ctx.c);
			ctx.h_ = ctx.g;
			ctx.g = ctx.f;
			ctx.f = ctx.e;
			ctx.e = ctx.d + t1;
			ctx.d = ctx.c;
			ctx.c = ctx.b;
			ctx.b = ctx.a;
			ctx.a = t1 + t2;
		}
		ctx.h[0] += ctx.a;
		ctx.h[1] += ctx.b;
		ctx.h[2] += ctx.c;
		ctx.h[3] += ctx.d;
		ctx.h[4] += ctx.e;
		ctx.h[5] += ctx.f;
		ctx.h[6] += ctx.g;
		ctx.h[7] += ctx.h_;
	}
	free(padded);
	return (format_hash(ctx.h, 8, 128, 16));
}
