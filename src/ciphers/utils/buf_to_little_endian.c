#include "ft_ssl.h"

void    buf_to_little_endian(void *data, size_t size)
{
    size_t     i;

    i = 0;
    while (i < size / 2)
    {
        ft_memswap(data + i, data + size - 1 - i, 1);
        ++i;
    }
}