#include "ft_ssl.h"

unsigned char   *format_hash(void *h, size_t tab_size, size_t format_size, size_t h_size)
{
	unsigned char	    *digest;
	char			    *tmp;
	size_t		    	i;
    unsigned long long  value;

	if (!(digest = (unsigned char *)malloc(sizeof(unsigned char) * (format_size + 1))))
		return (NULL);
	digest[format_size] = '\0';
	i = -1;
	while (++i < tab_size)
	{
        value = (h_size == 16) ? ((unsigned long *)h)[i] : ((unsigned int *)h)[i];
		if (!(tmp = itoa_base(value, 16, h_size)))
			return (NULL);
		memcpy(digest + i * h_size, tmp, h_size);
		free(tmp);
	}
	return (digest);
}