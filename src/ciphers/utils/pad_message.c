#include "ft_ssl.h"

unsigned char	*pad_message(unsigned char *message, size_t byte_len, size_t *padded_len, size_t block_size, size_t len_size)
{
	unsigned char	*padded;

	*padded_len = byte_len * 8 + 1;
	while (*padded_len % block_size != block_size - len_size)
		(*padded_len)++;
	*padded_len /= 8;
	if (!(padded = (unsigned char *)malloc(sizeof(unsigned char) * (*padded_len + (len_size / 8)))))
		return (NULL);
	memset(padded, 0x00, *padded_len);
	memcpy(padded, message, byte_len);
	padded[byte_len] = 0x80;
	byte_len *= 8;
	memcpy(padded + *padded_len, &byte_len, 8);
	return (padded);
}
