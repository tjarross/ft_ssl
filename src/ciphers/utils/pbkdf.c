#include <openssl/sha.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define IPAD	0x36
#define OPAD	0x5c

#define SHA_BLOCKSIZE	64	

void pbkdf1_sha1(const char *password, const char *salt, long iter, unsigned char dk[SHA_DIGEST_LENGTH])
{
    size_t			pwlen = strlen(password);
    size_t			dlen = pwlen + 8;
    unsigned char	*buf;

    if (dlen > SHA_DIGEST_LENGTH)
        buf = malloc(dlen);
    else
        buf = malloc(SHA_DIGEST_LENGTH);

    memcpy(buf, password, pwlen);
    strncpy((char *)buf + pwlen, salt, 8);

    while (iter-- > 0)
    {
        SHA1(buf, dlen, buf);
        dlen = SHA_DIGEST_LENGTH;
    }

    memcpy(dk, buf, SHA_DIGEST_LENGTH);
    free(buf);
}

void *memxor(void *dest, void *src, size_t len)
{
	size_t i;
	
	i = 0;
	while (i < len)
	{
		((int8_t *)dest)[i] ^= ((int8_t *)src)[i];
		++i;
	}
	return (dest);
}

void *memxorc(void *s, char c, size_t len)
{
	size_t i;
	
	i = 0;
	while (i < len)
	{
		((int8_t *)s)[i] ^= c;
		++i;
	}
	return (s);
}

uint32_t bswap32(uint32_t n)
{
  return (((n & 0x000000FF) << 24)
		| ((n & 0x0000FF00) <<  8)
		| ((n & 0x00FF0000) >>  8)
		| ((n & 0xFF000000) >> 24));
}

void hmac_sha1(unsigned char	*message,
				size_t			message_len,
				unsigned char	*key,
				size_t			key_len,
				unsigned char	out[SHA_DIGEST_LENGTH])
{
	uint8_t	padded_key[SHA_BLOCKSIZE] = {0};
	uint8_t	ipad_hash[SHA_DIGEST_LENGTH];
	uint8_t	ipadded_msg[SHA_BLOCKSIZE];
	uint8_t	opadded_msg[SHA_BLOCKSIZE];
	uint8_t	final_msg[sizeof(opadded_msg) + SHA_DIGEST_LENGTH];
	uint8_t	*tmp;
	size_t	tmp_len;

	memset(out, 0, SHA_DIGEST_LENGTH);
	// Checking key length
	if (key_len > SHA_BLOCKSIZE)
		SHA1(key, key_len, padded_key);
	else
		memcpy(padded_key, key, key_len);
	
	// 'Xoring' key with opad
	memcpy(opadded_msg, padded_key, sizeof(padded_key));
	memxorc(opadded_msg, OPAD, sizeof(opadded_msg));
	
	// 'Xoring' key with ipad
	memcpy(ipadded_msg, padded_key, sizeof(padded_key));
	memxorc(ipadded_msg, IPAD, sizeof(ipadded_msg));
	
	// Ipad Hash
	if (!(tmp = (uint8_t *)malloc(sizeof(uint8_t) * (sizeof(ipadded_msg) + message_len))))
		return ;
	tmp_len = sizeof(ipadded_msg) + message_len;

	memcpy(tmp, ipadded_msg, sizeof(ipadded_msg));
	memcpy(tmp + sizeof(ipadded_msg), message, message_len);
	SHA1(tmp, tmp_len, ipad_hash);
	free(tmp);
	
	// Final Hash
	memcpy(final_msg, opadded_msg, sizeof(opadded_msg));
	memcpy(final_msg + sizeof(opadded_msg), ipad_hash, sizeof(ipad_hash));
	SHA1(final_msg, sizeof(final_msg), out);
}

void f(unsigned char *password, size_t password_len, unsigned char *salt, size_t salt_len, uint32_t iter, uint32_t i, unsigned char xorsum[SHA_DIGEST_LENGTH])
{
	size_t			j;
	unsigned char	*tmp;
	size_t			tmp_len;
	unsigned char	u[SHA_DIGEST_LENGTH] = {0};
	unsigned char	u_tmp[SHA_DIGEST_LENGTH] = {0};

	memset(xorsum, 0, SHA_DIGEST_LENGTH);
	
	// Salt and i concatenation
	if (!(tmp = (unsigned char *)malloc(sizeof(unsigned char) * (salt_len + sizeof(i)))))
		exit(1);
	tmp_len = salt_len + sizeof(i);
	memcpy(tmp, salt, salt_len);
	i = bswap32(i); ///  most significant  bits first
	memcpy(tmp + salt_len, &i, sizeof(i));

	// First iteration
	hmac_sha1(tmp, tmp_len, password, password_len, u);
	memxor(xorsum, u, sizeof(u));

	// Rounds
	j = 1;
	while (j < iter)
	{
		memset(u_tmp, 0, sizeof(u_tmp));
		hmac_sha1(u, sizeof(u), password, password_len, u_tmp);
		memcpy(u, u_tmp, sizeof(u_tmp));
		memxor(xorsum, u, sizeof(u));
		++j;
	}
}

unsigned char *pbkdf2_sha1(unsigned char *password, size_t password_len, unsigned char *salt, size_t salt_len, int iter, size_t out_key_size)
{
	unsigned int	i;
	unsigned int	l;
	unsigned char	result[SHA_DIGEST_LENGTH];
	unsigned char	*key;
	unsigned char	*tmp;
	size_t			tmp_len;
	
	l = ceil(out_key_size / (float)SHA_DIGEST_LENGTH);
	
	if (!(key = (unsigned char *)malloc(sizeof(unsigned char) * out_key_size)))
		exit(1);

	i = 0;
	tmp = NULL;
	tmp_len = 0;
	while (i < l)
	{
		f(password, password_len, salt, salt_len, iter, i + 1, result);
		if (!(tmp = (unsigned char *)realloc(tmp, sizeof(unsigned char) * tmp_len + sizeof(result))))
			exit(1);
		memcpy(tmp + tmp_len, result, sizeof(result));
		tmp_len += sizeof(result);
		++i;
	}
	memcpy(key, tmp, out_key_size);
	free(tmp);
	return (key);
}
