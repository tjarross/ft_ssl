#include "ft_ssl.h"

void	print_infos(t_opts opts, int i, char *hash_digest)
{
	FILE *fp;

	if (opts.out_pathname[0] != '\0')
	{
		if (!(fp = fopen(opts.out_pathname, "w")))
		{
			printf("ft_ssl: %s: %s: No such file or directory\n", opts.cmd, opts.out_pathname);
			return ;
		}
	}
	else
		fp = stdout;
	if (opts.q == false)
	{
		if (opts.r == true)
		{
			if (opts.arg_type[i] != ARG_FILE)
				fprintf(fp, "%s \"%s\"\n", hash_digest, opts.arg[i]);
			else
				fprintf(fp, "%s %s\n", hash_digest, opts.arg[i]);
		}
		else
		{
			if (opts.arg_type[i] != ARG_FILE)
				fprintf(fp, "%s (\"%s\") = %s\n", opts.cmd, opts.arg[i], hash_digest);
			else
				fprintf(fp, "%s (%s) = %s\n", opts.cmd, opts.arg[i], hash_digest);
		}
	}
	else
	{
		fprintf(fp, "%s\n", hash_digest);
	}
	fclose(fp);
}

unsigned char	*handle_cmd(t_opts opts, unsigned char *str, size_t size)
{
	unsigned char	*digest;
	unsigned char	*b64_digest;

	if (strcmp(opts.cmd, "md5") == 0)
		return (md5(str, size));
	else if (strcmp(opts.cmd, "sha224") == 0)
		return (sha224(str, size));
	else if (strcmp(opts.cmd, "sha256") == 0)
		return (sha256(str, size));
	else if (strcmp(opts.cmd, "sha384") == 0)
		return (sha384(str, size));
	else if (strcmp(opts.cmd, "sha512") == 0)
		return (sha512(str, size));
	else if (strcmp(opts.cmd, "base64") == 0 && opts.mode == ENCRYPT)
		return (base64_encode(str, size, "base64"));
	else if (strcmp(opts.cmd, "base64url") == 0 && opts.mode == ENCRYPT)
		return (base64_encode(str, size, "base64url"));
	else if (strcmp(opts.cmd, "base64") == 0 && opts.mode == DECRYPT)
		return (base64_decode(str, &size));
	else if (strcmp(opts.cmd, "base64url") == 0 && opts.mode == DECRYPT)
		return (base64_decode(str, &size));
	else if (strcmp(opts.cmd, "des-ecb") == 0)
	{
		t_des_ctx	ctx = {0};

		if (opts.a == true && opts.mode == DECRYPT)
		{
			b64_digest = base64_decode(str, &size);
			free(str);
			str = b64_digest;
		}
		ctx.algorithm = opts.cmd;
		ctx.mode = opts.mode;
		ctx.key = *(uint64_t *)opts.pbkdf2_key;
		ctx.padding = true;
		ctx.plaintext = str;
		ctx.plaintext_size = size;
		ctx.padding = !opts.nopad;
		digest = des_ecb(&ctx);
		digest[ctx.cipheredtext_size] = '\0';
		if (opts.a == true && opts.mode == ENCRYPT)
		{
			b64_digest = base64_encode(digest, ctx.cipheredtext_size, "base64");
			free(digest);
			digest = b64_digest;
		}
		return (digest);
	}
	else
		return (NULL);
}

void			release_arguments(t_opts opts)
{
	size_t	i;

	i = 0;
	while (i < opts.nb_arg)
		free(opts.arg[i++]);
	free(opts.arg);
	free(opts.arg_type);
	free(opts.enc_passwd);
	free(opts.iv);
	free(opts.pbkdf2_key);
}

char			*get_input(size_t *out_size)
{
	char	*str;
	char	buf[4096];
	int		ret;

	*out_size = 0;
	while ((ret = read(0, buf, sizeof(buf))) > 0)
	{
		str = realloc(str, *out_size + ret + 1);
		str[*out_size + ret] = '\0';
		memcpy(str + *out_size, buf, ret);
		*out_size += ret;
	}
	return (ret >= 0 ? str : NULL);
}

int				argument_checker(int ac, char **av)
{
	t_parser		arguments[] =
	{
		{"md5 sha224 sha256 sha384 sha512", "*s. p q r o.", "string p quiet reverse out", true},
		{"base64 base64url", "1(e|d) i. o. p q", "encrypt decrypt in out p quiet", false},
		{"des-ecb", "1(e|d) a i. o. k. K. *s. v. P p nopad S.", "encrypt decrypt a in out key pass string iv P p nopad salt", false},
		{NULL, NULL, NULL, false}
	};

	if (ac <= 1)
		return (1);

	return (check_opts(av, ac, arguments));
}

int				main(int ac, char **av)
{
	t_opts			opts;
	size_t			i;
	unsigned char	*digest;
	char			*str;
	char			*file;
	size_t			size;

	i = -1;
	size = 0;
	if (argument_checker(ac, av))
		return (EXIT_FAILURE);

	opts = parse_opts(ac, av);
	if (opts.p == true)
	{
		str = get_input(&size);
		printf("%s", str);
		digest = handle_cmd(opts, (unsigned char *)str, size);
		printf("%s\n", (char *)digest == NULL ? "NULL" : (char *)digest);
		free(str);
		free(digest);
	}
	while (++i < opts.nb_arg)
	{
		if (opts.arg_type[i] == ARG_FILE)
		{
			int ret;
			if ((ret = get_file(opts.arg[i], &size, &file)))
			{
				printf("ft_ssl: %s: %s: No such file or directory\n", opts.cmd, opts.arg[i]);
				continue ;
			}
			else
			{
				digest = handle_cmd(opts, (unsigned char *)file, size);
				free(file);
			}
		}
		else
		{
			digest = handle_cmd(opts, (unsigned char *)opts.arg[i], strlen(opts.arg[i]));
		}
		print_infos(opts, i, (char *)digest);
		free(digest);
	}
	
	release_arguments(opts);
	return (EXIT_SUCCESS);
}
