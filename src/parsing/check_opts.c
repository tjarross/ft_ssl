#include "ft_ssl.h"	

int check_opts(int ac, char **av)
{
	int		i;
	int		j;

	i = 0;
	if (ac < 2)
		return (1);
	while (++i < ac)
	{
		if (i == 1)
		{
			if (strcmp(av[i], "md5") == 0)
				continue ;
			else if (strcmp(av[i], "sha224") == 0)
				continue ;
			else if (strcmp(av[i], "sha256") == 0)
				continue ;
			else if (strcmp(av[i], "sha384") == 0)
				continue ;
			else if (strcmp(av[i], "sha512") == 0)
				continue ;
			else if (strcmp(av[i], "base64") == 0
			||		strcmp(av[i], "base64url") == 0)
			{
				if (i + 1 < ac
				&& (strcmp(av[i + 1], "-e") == 0 || strcmp(av[i + 1], "-d") == 0))
				{
					++i;
					continue ;
				}
				return (1); 
			}
			else
				return (1);
		}
		else if (av[i][0] == '-')
		{
			j = 0;
			while (av[i][++j])
				if (av[i][j] != 'p' && av[i][j] != 'r'
				&&  av[i][j] != 'q' && av[i][j] != 's')
					return (1);
			if (strchr(av[i], 's'))
			{
				if (i != ac - 1)
					++i;
				else
					return (1);
			}
		}
	}
	return (0);
}
