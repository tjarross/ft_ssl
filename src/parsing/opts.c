#include "ft_ssl.h"

typedef struct		s_lst
{
	int				pos;
	char			**short_options;
	char			**long_options;
	int				nb_authorized;
	int				nb_counted;
	int				has_argument;
	struct s_lst	*next;
}					t_lst;

typedef enum		e_index
{
	SHORT,
	LONG
}					t_index;

static int non_null(const t_parser opts)
{
	return (opts.cmd != NULL && opts.short_options != NULL && opts.long_options != NULL);
}

static int check_cmd(const char *cmd, t_parser *opts, int *opts_index)
{
	int		i;
	int		j;
	char	*ptr;

	if (strchr(cmd, ' '))
		return (1);
	i = -1;
	while (non_null(opts[++i]))
	{
		j = 0;
		while (opts[i].cmd[j])
		{
			if (strncmp(opts[i].cmd + j, cmd, strlen(cmd)) == 0)
				if (opts[i].cmd[j + strlen(cmd)] == ' '
				||  opts[i].cmd[j + strlen(cmd)] == '\0')
				{
					*opts_index = i;
					return (0);
				}
			if ((ptr = strchr(opts[i].cmd + j, ' ')))
				j += ptr - (opts[i].cmd + j) + 1;
			else
				break ;
		}
	}
	return (1);
}

static int	check_option(const char *option, int *option_pos, t_lst *lst, bool is_permissive)
{
	t_lst	*tmp;
	int		i;
	bool	option_found;

	tmp = lst;
	option_found = false;
	if (option[0] == '-' && strlen(option) < 2)
	{
		printf("ERR : strlen(option) < 2\n");
		return (1);
	}
	while (tmp)
	{
		i = 0;
		while (tmp->short_options[i])
		{
			if (strcmp(option + 1, tmp->short_options[i]) == 0
			||  strcmp(option + 2, tmp->long_options[i]) == 0)
			{
				option_found = true;
				break ;
			}
			++i;
		}
		if (option_found)
		{
			tmp->nb_counted++;
			if (tmp->pos != 0
			&&  tmp->pos != (*option_pos - 1))
			{
				printf("ERR : option \"%s\" invalid pos %d\n", option, *option_pos - 1);
				return (1);
			}
			if (tmp->nb_authorized != 0
			&&  tmp->nb_counted > tmp->nb_authorized)
			{
				printf("ERR : too much options\n");
				return (1);
			}
			if (tmp->has_argument)
				(*option_pos)++;
			return (0);
		}
		tmp = tmp->next;
	}
	if (is_permissive)
		return (option[0] != '-' ? 0 : 1);
	if (!option_found)
		printf("ERR : option not found %s\n", option);
	return (!option_found);
}

static int		verify_lst(t_lst *lst)
{
	t_lst *tmp;

	tmp = lst;
	while (tmp)
	{
		if (tmp->pos != 0
		&&  tmp->nb_counted != 1)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

static void	delete_lst(t_lst *lst)
{
	t_lst *tmp;

	tmp = lst;
	while (lst)
	{
		tmp = tmp->next;
		free_split(lst->short_options);
		free_split(lst->long_options);
		free(lst);
		lst = tmp;
	}
}

void	print_lst(t_lst *lst)
{
	int		i;
	t_lst	*tmp;

	i = 0;
	tmp = lst;
	while (tmp)
	{
		printf("Maillon %d\n", i);
		printf("\tPos = %d\n", tmp->pos);
		printf("\tOptions\n");
		for (int i = 0; tmp->short_options[i]; ++i)
			printf("\t\t%s / %s\n", tmp->short_options[i], tmp->long_options[i]);
		printf("\tHas Argument = %d\n", tmp->has_argument);
		tmp = tmp->next;
		++i;
	}
}

static void	fill_list(t_lst *lst, char **short_split_str, char **long_split_str, int split_index[2])
{
	int			str_index;
	int			i;
	int			nb_split;
	char		*tmp;

	str_index = 0;
	i = 0;

	// Pos
	if (short_split_str[split_index[SHORT]][0] != '*')
	{
		lst->nb_authorized = 1;
		if (isdigit(short_split_str[split_index[SHORT]][0]))
		{
			lst->pos = atoi(short_split_str[split_index[SHORT]]);
			while (isdigit(short_split_str[split_index[SHORT]][str_index]))
				++str_index;
		}
		else
		{
			lst->pos = 0;
		}
	}
	else
	{
		lst->pos = 0;
		lst->nb_authorized = 0;
		str_index = 1;
	}

	// Has argument
	lst->has_argument = strrchr(short_split_str[split_index[SHORT]], '.') ? 1 : 0;

	// Short/Long Options
	if (short_split_str[split_index[SHORT]][str_index] == '(')
	{
		tmp = strcdup(short_split_str[split_index[SHORT]] + str_index + 1, ')');
		lst->short_options = strsplit(tmp, '|');
		free(tmp);
		nb_split = splitlen(lst->short_options);
		lst->long_options = (char **)malloc(sizeof(char *) * (nb_split + 1));
		lst->long_options[nb_split] = NULL;
		i = -1;
		while (++i < nb_split)
			lst->long_options[i] = strdup(long_split_str[split_index[LONG] + i]);
		split_index[SHORT]++;
		split_index[LONG] += nb_split;
	}
	else
	{
		lst->short_options = (char **)malloc(sizeof(char *) * (1 + 1));
		lst->short_options[0] = strcdup(short_split_str[split_index[SHORT]] + str_index, (lst->has_argument ? '.' : '\0'));
		lst->short_options[1] = NULL;
		split_index[SHORT]++;

		lst->long_options = (char **)malloc(sizeof(char *) * (1 + 1));
		lst->long_options[0] = strdup(long_split_str[split_index[LONG]]);
		lst->long_options[1] = NULL;
		split_index[LONG]++;
	}

	lst->nb_counted = 0;

	lst->next = NULL;
}

static int		make_list(const char *short_options, const char *long_options, t_lst **lst)
{
	char	**short_split;
	char	**long_split;
	int		index[2] = {0};
	t_lst	*tmp;

	short_split = strsplit(short_options, ' ');
	long_split = strsplit(long_options, ' ');
	*lst = (t_lst *)malloc(sizeof(t_lst));
	tmp = *lst;
	while (long_split[index[LONG]])
	{
		fill_list(tmp, short_split, long_split, index);
		if (long_split[index[LONG]] != NULL)
		{
			tmp->next = (t_lst *)malloc(sizeof(t_lst));
			tmp = tmp->next;
		}
	}
	free_split(short_split);
	free_split(long_split);
	return (0);
}

int check_opts(char **options, int nb_options, t_parser *opts)
{
	int		i;
	int		ret;
	int		opts_index;
	t_lst	*lst;

	if (nb_options <= 0 || !options || !opts)
		return (1);

	if (check_cmd(options[1], opts, &opts_index))
		return (1);

	i = 2;
	make_list(opts[opts_index].short_options, opts[opts_index].long_options, &lst);
//	print_lst(lst);
	while (i < nb_options)
	{
		ret = check_option(options[i], &i, lst, opts[opts_index].permissive);
		if (ret != 0)
			printf("ERR : check_options()\n");
		if (i >= nb_options)
		{
			printf("ERR : i >= nb_options\n");
			ret = 1;
		}
		if (ret != 0)
			break ;
		++i;
	}
	if (verify_lst(lst))
	{
		printf("ERR : verify_lst()\n");
		ret = 1;
	}
	delete_lst(lst);
	return (ret != 0);
}
