#include "ft_ssl.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>

void	display_infos(t_opts opts)
{
	size_t	i;

	i = -1;
	printf("Command:\n");
	printf("\t%s\n", opts.cmd);
	printf("Mode:\n");
	printf("\t%s\n", (opts.mode == 0) ? "NO_MODE" : ((opts.mode == 1) ? "ENCRYPT" : "DECRYPT"));
	printf("Options:\n");
	printf("\t\"-p\" =     %s\n", (opts.p ? "true" : "false"));
	printf("\t\"-q\" =     %s\n", (opts.q ? "true" : "false"));
	printf("\t\"-r\" =     %s\n", (opts.r ? "true" : "false"));
	printf("\t\"-a\" =     %s\n", (opts.a ? "true" : "false"));
	printf("\t\"-P\" =     %s\n", (opts.P ? "true" : "false"));
	printf("\t\"-nopad\" = %s\n", (opts.nopad ? "true" : "false"));
	printf("\t\"-i\" =     \"%s\"\n", opts.in_pathname);
	printf("\t\"-o\" =     \"%s\"\n", opts.out_pathname);
	printf("\t\"-k\" =     %s\n", opts.pbkdf2_key);
	printf("\t\"-S\" =     %s\n", opts.salt);
	printf("\t\"-K\" =     %s\n", opts.enc_passwd);
	printf("\t\"-v\" =     %s\n\n", opts.iv);
	if (opts.nb_arg > 0)
	{
		printf("Arguments:\n");
		while (++i < opts.nb_arg)
			printf("\t%s = \"%s\"\n", (opts.arg_type[i] != ARG_STRING) ? "FILE" : "STR ", opts.arg[i]);
	}
}

int		is_hash(const char *str)
{
	const char	hash_list[][MAX_CMD_SIZE] = SUPPORTED_HASH_LIST;
	int			i;

	i = 0;
	while ((size_t)i < NB_ELEM_IN_LIST(hash_list))
	{
		if (strcmp(hash_list[i], str) == 0)
			return (1);
		++i;
	}
	return (0);
}

int		is_cipher(const char *str)
{
	const char	cipher_list[][MAX_CMD_SIZE] = SUPPORTED_CIPHER_LIST;
	int			i;

	i = 0;
	while ((size_t)i < NB_ELEM_IN_LIST(cipher_list))
	{
		if (strcmp(cipher_list[i], str) == 0)
			return (1);
		++i;
	}
	return (0);
}

int		nstring_match(const char *str, int nb, ...)
{
	va_list	args;
	int		i;

	va_start(args, nb);

	i = 0;
	while (i < nb)
	{
		if (strcmp(str, va_arg(args, const char *)) == 0)
		{
			va_end(args);
			return (1);
		}
		++i;
	}
	va_end(args);
	return (0);
}

t_mode get_cipher_mode(const char *str)
{
	if (nstring_match(str, 2, "-e", "--encrypt"))
		return (ENCRYPT);
	if (nstring_match(str, 2, "-d", "--decrypt"))
		return (DECRYPT);
	return (NO_MODE);
}

int				ishex(char c)
{
	return (isdigit(c) || (tolower(c) >= 'a' && tolower(c) <= 'f'));
}

int				is_hex_string(const char *str)
{
	int	i;

	i = 0;
	if (strlen(str) % 2 != 0)
		return (0);
	while (str[i])
	{
		if (!ishex(str[i]))
			return (0);
		++i;
	}
	return (1);
}

unsigned char	*str_to_hex(const char *s, size_t *size)
{
	unsigned char	*str;
	int				i, j;

	if (!is_hex_string(s))
		return (NULL);

	if (!(str = (unsigned char *)strdup(s)))
		return (NULL);
	
	i = 0;
	j = 0;
	while (str[j])
	{
		unsigned char tmp;
		tmp  = (isdigit(str[j + 0]) ? (str[j + 0] - '0') : tolower(str[j + 0]) - 'a' + 10) << 4;
		tmp |= (isdigit(str[j + 1]) ? (str[j + 1] - '0') : tolower(str[j + 1]) - 'a' + 10);
		str[i++] = tmp;
		j += 2;
	}
	*size = i;
	return (str);
}

void	parse_flag(char **av, int *i, t_opts *opts)
{
	if (nstring_match(av[*i], 2, "-i", "--in"))
		strncpy(opts->in_pathname, av[++(*i)], PATH_MAX);
	else if (nstring_match(av[*i], 2, "-o", "--out"))
		strncpy(opts->out_pathname, av[++(*i)], PATH_MAX);
	else if (nstring_match(av[*i], 2, "-k", "--key"))
		opts->pbkdf2_key = str_to_hex(av[++(*i)], &opts->pbkdf2_key_len);
	else if (nstring_match(av[*i], 2, "-S", "--salt"))
		opts->salt = strdup(av[++(*i)]);
	else if (nstring_match(av[*i], 2, "-K", "--pass"))
		opts->enc_passwd = strdup(av[++(*i)]);
	else if (nstring_match(av[*i], 2, "-v", "--iv"))
		opts->iv = strdup(av[++(*i)]);
	else if (nstring_match(av[*i], 1, "-a"))
		opts->a = true;
	else if (nstring_match(av[*i], 1, "-p"))
		opts->p = true;
	else if (nstring_match(av[*i], 1, "-P"))
		opts->P = true;
	else if (nstring_match(av[*i], 1, "-q"))
		opts->q = true;
	else if (nstring_match(av[*i], 1, "-r"))
		opts->r = true;
	else if (nstring_match(av[*i], 1, "-nopad"))
		opts->nopad = true;
	else if (nstring_match(av[*i], 2, "-s", "--string"))
	{
		opts->arg = realloc(opts->arg, sizeof(char *) * (opts->nb_arg + 1));
		opts->arg[opts->nb_arg] = strdup(av[*i + 1]);
		opts->arg_type = realloc(opts->arg_type, sizeof(t_arg) * (opts->nb_arg + 1));
		opts->arg_type[opts->nb_arg] = ARG_STRING;
		++opts->nb_arg;
		++(*i);
	}
}

void	get_last_options(t_opts *opts)
{
	char str[MAX_PWD_SIZE + 1] = {0};

	srand(time(NULL));
	if (is_cipher(opts->cmd))
	{
		if (!opts->salt)
		{
			opts->salt = itoa_base(((unsigned long)rand() << 32) | (unsigned long)rand(), 16, 16);
			printf("Salted__%s\n", opts->salt);
		}
		if (!opts->pbkdf2_key)
		{
			readpassphrase("enter des-ecb encryption password:", str, sizeof(str) - 1, RPP_REQUIRE_TTY | RPP_ECHO_OFF);
			opts->pbkdf2_key = pbkdf2_sha1((unsigned char *)str, strlen(str), (unsigned char *)opts->salt, strlen(opts->salt), 2048, 8);
		}
	}
}

t_opts	parse_opts(int ac, char **av)
{
	t_opts	opts;
	int		i;

	i = 1;
	bzero(&opts, sizeof(t_opts));
	if (is_hash(av[i]))
	{
		strncpy(opts.cmd, av[i], MAX_CMD_SIZE);
		++i;
	}
	else if (is_cipher(av[i]))
	{
		strncpy(opts.cmd, av[i], MAX_CMD_SIZE);
		opts.mode = get_cipher_mode(av[i + 1]);
		i += 2;
	}
	while (i < ac)
	{
		if (av[i][0] == '-')
		{
			parse_flag(av, &i, &opts);
		}
		else
		{
			opts.arg = realloc(opts.arg, sizeof(char *) * (opts.nb_arg + 1));
			opts.arg[opts.nb_arg] = strdup(av[i]);
			opts.arg_type = realloc(opts.arg_type, sizeof(opts.arg_type) * (opts.nb_arg + 1));
			opts.arg_type[opts.nb_arg] = ARG_FILE;
			++opts.nb_arg;
		}
		++i;
	}
	get_last_options(&opts);

	display_infos(opts);
	return (opts);
}
