#include "ft_ssl.h"

static int		ft_inbase(char c, int base)
{
	c = ft_toupper(c);
	if (base <= 10)
		return (c >= '0' && c <= '9');
	return ((c >= '0' && c <= '9') || (c >= 'A' && c <= ('A' + base - 10)));
}

int				atoi_base(char *str, int base, size_t len)
{
	int		value;
	int		sign;
	char	c;

	value = 0;
	if (base <= 1 || base > 36)
		return (0);
	while (*str == ' ' || *str == '\t' || *str == '\n' || *str == '\f'
			|| *str == '\r' || *str == '\v')
	{
		if (--len <= 0)
			break ;
		str++;
	}
	if (len <= 0)
		return (0);

	sign = (*str == '-') ? -1 : 1;
	if (*str == '-' || *str == '+')
	{
		--len;
		str++;
	}
	while (ft_inbase(*str, base) && len > 0)
	{
		c = ft_toupper(*str);
		if (c - 'A' >= 0)
			value = value * base + (c - 'A' + 10);
		else
			value = value * base + (c - '0');
		str++;
		--len;
	}
	return (value * sign);
}
