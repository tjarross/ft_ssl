#include "ft_ssl.h"

int		get_file(char *pathname, size_t *len, char **str)
{
	int		fd;
	size_t	fsize;
	int		ret;
	char	buf[4096];

	fsize = 0;
	*str = NULL;
	if ((fd = open(pathname, O_RDONLY)) == -1)
		return (1);
	while ((ret = read(fd, buf, sizeof(buf))) > 0)
	{
		*str = realloc(*str, fsize + ret + 1);
		(*str)[fsize + ret] = '\0';
		memcpy(*str + fsize, buf, ret);
		fsize += ret;
	}
	if (ret < 0)
	{
		free(str);
		return (3);
	}
	if (close(fd) == -1)
	{
		free(str);
		return (2);
	}
	*len = fsize;
	return (0);
}
