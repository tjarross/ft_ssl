#include "ft_ssl.h"

char		*itoa_base(unsigned long long value, int base, size_t out_len)
{
	int		i;
	char	*nbr;

	if (!(nbr = (char *)malloc(sizeof(nbr) * out_len)))
		return (NULL);
	memset(nbr, '0', out_len);
	nbr[out_len] = '\0';
	i = out_len;
	while (i-- > 0)
	{
		nbr[i] = (value % base) + (value % base > 9 ? 'a' - 10 : '0');
		value = value / base;
	}
	return (nbr);
}