#include "ft_ssl.h"

char	*strccpy(char *dest, const char *src, char c)
{
	int i;

	if (!dest || !src)
		return (NULL);
	i = 0;
	while (src[i] != c && src[i])
	{
		dest[i] = src[i];
		++i;
	}
	dest[i] = '\0';
	return (dest);
}