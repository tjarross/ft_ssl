#include "ft_ssl.h"

char	*strcdup(const char *s1, char c)
{
	char	*s2;
	char	*ptr;

	ptr = strchr(s1, c);
	if (!(s2 = (char *)malloc(sizeof(char) * ((ptr == NULL) ? (int)strlen(s1) : (ptr - s1)) + 1)))
		return (NULL);
	return (strccpy(s2, s1, c));
}