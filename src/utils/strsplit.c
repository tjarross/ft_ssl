#include "ft_ssl.h"

void		free_split(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
		free(tab[i++]);
	free(tab);
}

int			splitlen(char **split)
{
	int		i;

	i = 0;
	while (split[i])
		++i;
	return (i);
}

static int	count_words(const char *str, char delim)
{
	int		i;
	int		word;

	i = -1;
	word = 1;
	while (str[++i])
		if (str[i] == delim)
			++word;
	return (word);
}

static int	get_wordlen(const char *str, char delim)
{
	int		i;

	i = 0;
	while (str[i] != delim && str[i])
		++i;
	return (i);
}

char		**strsplit(const char *str, char delim)
{
	char	**dest;
	int		words;
	int		i;
	int		j;
	int		word_len;

	i = 0;
	j = -1;
	if (!str || !isascii(delim) || !(words = count_words(str, delim)))
		return (NULL);
	if (!(dest = (char **)malloc(sizeof(char *) * (words + 1))))
		return (NULL);
	dest[words] = NULL;
	while (++j < words)
	{
		word_len = get_wordlen(str + i, delim);
		if (!(dest[j] = (char *)malloc(sizeof(char) * (word_len + 1))))
			return (NULL);
		dest[j] = strncpy(dest[j], str + i, word_len);
		i += word_len + 1;
	}
	return (dest);
}