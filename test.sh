arg=""
stdin=""
status=""

test()
{
	arg="$3"
	stdin="$2"
	status="$1"
	if [ -z $stdin ]	# if $stdin is empty
	then
		output=`./ft_ssl $arg`
	else
		output=`echo "$stdin" | ./ft_ssl $arg`
	fi

	ret=`echo $?`
	if [ $ret -eq $status ]
	then
		if [ $ret -eq 0 ]
		then
			#test stdout of command
			echo "TEST OK"
		else
			echo "TEST OK"
		fi
	else
		echo "TEST NOK"
	fi
}

test 0 "abc" "md5 -s abc"
test 0 "abc" "md5 -p"